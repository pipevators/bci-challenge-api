# BCI Challenge API

## Requisitos
- Java 8
- Gradle 6.7.0+

## Uso

Clonar proyecto

```bash
git clone git@gitlab.com:pipevators/bci-challenge-api.git
```

Entrar a la carpeta del proyecto y ejecutar

```bash
cd bci-challenge-api
gradle bootRun
```


## Documentación 
Para la documentación de la API se usó Swagger 

Una vez levantado el proyecto ir a [http://localhost:8080/swagger-ui.html#](http://localhost:8080/swagger-ui.html#)

Se pueden encontrar los diagramas de componente y secuencia dentro del proyecto en: documents/diagrams

