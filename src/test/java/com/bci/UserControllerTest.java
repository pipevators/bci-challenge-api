package com.bci;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.net.URISyntaxException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.bci.controller.UsersController;
import com.bci.service.UserService;

@TestMethodOrder(OrderAnnotation.class)
@SpringBootTest
@AutoConfigureMockMvc
class UserControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Mock
	private UserService userService;

	@InjectMocks
	private UsersController controllerUnderTest;

	@BeforeEach
	void setMockOutput() throws IOException, URISyntaxException {

	}

	@Test
	@DisplayName("Create users successfully")
	@Order(1)
	void createUser() throws Exception {

		String json = "{\"name\":\"Juan Rodriguez\",\"email\":\"juan@aaa.com\",\"password\":\"aLL22\",\"phones\":[{\"number\":\"1234567\",\"citycode\":\"1\",\"contrycode\":\"57\"}]}";
		mockMvc.perform(post("/users")//
				.accept(MediaType.APPLICATION_JSON)//
				.contentType(MediaType.APPLICATION_JSON)//
				.content(json))//
				.andExpect(status().isCreated());

		json = "{\"name\":\"Juan Rodriguez\",\"email\":\"juan@rodriguez.org\",\"password\":\"HunTer22\",\"phones\":[{\"number\":\"1234567\",\"citycode\":\"1\",\"contrycode\":\"57\"}]}";
		mockMvc.perform(post("/users")//
				.accept(MediaType.APPLICATION_JSON)//
				.contentType(MediaType.APPLICATION_JSON)//
				.content(json))//
				.andExpect(status().isCreated());

		json = "{\"name\":\"Felipe Rodriguez\",\"email\":\"juan@gmail.com\",\"password\":\"user22GR\",\"phones\":[{\"number\":\"123\",\"citycode\":\"1\",\"contrycode\":\"44\"}]}";
		mockMvc.perform(post("/users")//
				.accept(MediaType.APPLICATION_JSON)//
				.contentType(MediaType.APPLICATION_JSON)//
				.content(json))//
				.andExpect(status().isCreated());
	}

	@Test
	@DisplayName("Create user with an existing email")
	@Order(2)
	void createUserWithExistingEmail() throws Exception {

		String json = "{\"name\":\"Miguel Rodriguez\",\"email\":\"juan@aaa.com\",\"password\":\"aLL22\",\"phones\":[{\"number\":\"1234567\",\"citycode\":\"1\",\"contrycode\":\"57\"}]}";
		mockMvc.perform(post("/users")//
				.accept(MediaType.APPLICATION_JSON)//
				.contentType(MediaType.APPLICATION_JSON)//
				.content(json))//
				.andExpect(status().isConflict());
	}

	@Test
	@DisplayName("Get a user of a non existing ID")
	@Order(3)
	void getUserByNonExistingId() throws Exception {
		mockMvc.perform(get("/users/1")//
				.accept(MediaType.APPLICATION_JSON)//
				.contentType(MediaType.APPLICATION_JSON)//
				.content(""))//
				.andExpect(status().isNotFound());
	}

}
