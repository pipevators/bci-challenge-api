package com.bci.exception;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * Error class to hold information about errors
 * 
 * @author pipe
 *
 */
@Data
@Builder
@AllArgsConstructor
public class BCIError {

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
	private LocalDateTime timestamp;
	private String mensaje;


	private BCIError() {
		timestamp = LocalDateTime.now();
	}


	public BCIError(String mensaje, Throwable ex) {
		this();
		this.mensaje = mensaje;
	}

}
