package com.bci.service;

import com.bci.dto.UserRequestDTO;
import com.bci.dto.UserResponseDTO;

public interface UserService {

	/**
	 * Add user to database
	 * 
	 * @param user
	 * @return
	 */
	public UserResponseDTO add(UserRequestDTO user);

	/**
	 * Get user by ID
	 * 
	 * @param id
	 * @return
	 */
	public UserResponseDTO getById(String id);

	/**
	 * Find a user by email
	 * 
	 * @param email
	 * @return
	 */
	public UserResponseDTO findByEmail(String email);
}
