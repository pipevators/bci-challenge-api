package com.bci.service.impl;

import com.bci.dto.UserRequestDTO;
import com.bci.dto.UserResponseDTO;
import com.bci.entity.PhoneEntity;
import com.bci.entity.UserEntity;
import com.bci.exception.NotFoundException;
import com.bci.exception.ResourceAlreadyExistException;
import com.bci.repository.UserRepository;
import com.bci.service.UserService;
import com.bci.utils.JwtUtils;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Log4j2
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository repository;

    @Override
    public UserResponseDTO add(UserRequestDTO user) {
        Optional<UserEntity> userEmail = repository.findByEmail(user.getEmail());
        if (userEmail.isPresent()) {
            throw new ResourceAlreadyExistException("Correo ya registrado");
        }

        List<PhoneEntity> phones = new ArrayList<>();
        if (user.getPhones() != null) {
            phones = user.getPhones().stream().map(x -> PhoneEntity.builder()
                    .number(x.getNumber())
                    .cityCode(x.getCityCode())
                    .countryCode(x.getCountryCode())
                    .build())
                    .collect(Collectors.toList());
        }

        UserEntity entity = UserEntity.builder()
                .name(user.getName())
                .email(user.getEmail())
                .password(user.getPassword())
                .phones(phones)
                .token(JwtUtils.createToken(user.getEmail()))
                .build();
        log.info("Saving user: {}", entity);
        repository.save(entity);

        Optional<UserEntity> userCreated = repository.findByEmail(user.getEmail());
        if (userCreated.isPresent()) {
            return userToDto(userCreated.get());
        }

        throw new RuntimeException("Error al crear el usuario");
    }

    @Override
    public UserResponseDTO getById(String id) {
        log.info("Get user by id");
        Optional<UserEntity> user = repository.findById(id);
        if (user.isPresent()) {
            log.info("User found with id: {} ", id);
            return userToDto(user.get());
        }
        throw new NotFoundException("Usuario no encontrado");
    }

    @Override
    public UserResponseDTO findByEmail(String email) {
        Optional<UserEntity> user = repository.findByEmail(email);
        if (user.isPresent()) {
            log.info("User found with email: {} ", email);
            return userToDto(user.get());
        }
        throw new NotFoundException("Usuario no encontrado");
    }

    public UserResponseDTO userToDto(UserEntity user) {
        return UserResponseDTO.builder()
                .id(user.getId())
                .created(user.getCreated())
                .modified(user.getModified())
                .lastLogin(user.getLastLogin())
                .token(user.getToken())
                .isActive(user.getIsActive())
                .build();
    }

}
