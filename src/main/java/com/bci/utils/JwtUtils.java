package com.bci.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;

public class JwtUtils {

	/**
	 * Private constructor
	 * 
	 */
	private JwtUtils() {
	}

	private static final String SECRET = "supersecret";
	private static final String ISSUER = "auth0";
	private static final String CLAIM = "claim";

	public static String createToken(String data) {
		return JWT.create().withIssuer(ISSUER)//
				.withClaim(CLAIM, data)//
				.sign(Algorithm.HMAC256(SECRET));
	}

	public static String verifyToken(String token) {
		JWTVerifier verifier = JWT.require(Algorithm.HMAC256(SECRET)).withIssuer(ISSUER).build();
		DecodedJWT jwt = verifier.verify(token);
		String data = jwt.getClaim(ISSUER).asString();
		return String.valueOf(data);
	}
}
