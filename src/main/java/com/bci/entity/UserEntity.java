package com.bci.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Table(name = "UserEntity")
@Entity
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserEntity {

	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
	private String id;

	@Column(name = "name")
	private String name;
	@Column(name = "password")
	private String password; // TODO: save password with encrypt algorithm
	@Column(name = "email")
	private String email;
	@Column(name = "created")
	private Date created;
	@Column(name = "modified")
	private Date modified;
	@Column(name = "lastLogin")
	private Date lastLogin;
	@Column(name = "isActive")
	@Builder.Default
	private Boolean isActive = true;
	@Column(name = "token")
	private String token;

	@OneToMany(targetEntity = PhoneEntity.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "phone_id", referencedColumnName = "id")
	private List<PhoneEntity> phones;

	@PreUpdate
	private void onLastLogin() {
		lastLogin = new Date();
	}

	@PrePersist
	private void onCreated() {
		lastLogin = modified = created = new Date();

	}

}
