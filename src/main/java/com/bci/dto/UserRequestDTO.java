package com.bci.dto;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @author pipe
 *
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserRequestDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	@NotNull(message = "Nombre requerido")
	private String name;
	@Email(message = "Email invalido")
	@NotNull(message = "Email requerido")
	private String email;
	@Pattern(regexp = "^(?=.*[A-Z])(?=.*\\d.*\\d)[^\\s]{4,}$", message = "La clave debe tener al menos una mayuscula, letras minúsculas, y dos numeros)")
	@NotNull(message = "Password requerido")
	private String password;

	private List<PhoneDTO> phones;

}
