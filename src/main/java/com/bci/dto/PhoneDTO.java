package com.bci.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class PhoneDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	@NotNull(message = "Número requerido")
	private String number;
	@JsonProperty(value = "citycode")
	@NotNull(message = "Codigo de cuidad requerido")
	private String cityCode;
	@JsonProperty(value = "contrycode")
	@NotNull(message = "Codigo de país requerido")
	private String countryCode;

}
