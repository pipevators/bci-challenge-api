package com.bci.controller;

import com.bci.dto.UserRequestDTO;
import com.bci.dto.UserResponseDTO;
import com.bci.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Controlador para Usuarios
 *
 * @author pipe
 */
@Api(value = "Operations about users")
@RestController
@Log4j2
@RequestMapping("/users")
public class UsersController {

    @Autowired
    private UserService userService;

    @ApiOperation(value = "Get user by ID")
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody
    UserResponseDTO getUserById(@PathVariable String id) {
        log.info("Procesing getUserById, with id: {}", id);
        return userService.getById(id);
    }

    @ApiOperation(value = "Creates a user")
    @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public @ResponseBody
    UserResponseDTO addUser(@RequestBody @Valid UserRequestDTO user) {
        log.info("Procesing addUser, with user: {}", user);
        return userService.add(user);
    }

}
